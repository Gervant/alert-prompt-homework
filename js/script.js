//переменная объявленная с const получает значение которое в последствии нельзя изменить, а значение переменной объявленной с помощью const - можно.
//var это устаревший способ объявления переменной к которой можно обратиться из любой части кода, чего нельзя сделать с let и const.
let userName = null;
let userAge = null
do {
    userName = prompt("Type your name: Please, use only text.")
    userAge = +prompt("Type your age: Please, use only numbers.")
}
while (!userName || !userAge || Number.isNaN(userAge))

if (userAge < 18) {
    alert("You are not allowed to visit this website!");
    }
    else{
        if (18 <= userAge && userAge <=22) {
            confirmation = confirm("Are you sure you want to continue?");
            if (confirmation) {
                alert(`Welcome ${userName}`);
            } else {
                alert("You are not allowed to visit this website!");
            }
        }
        else {
            alert(`Welcome ${userName}`)
            }
     }
